+++
title="Vorstellen und Kennenlernen"
weight=2
+++

<header class="bg-green">

   <b>Vorstellen und Kennenlernen</b>

</header>


    {{< div  class="wrap size-80" >}}
    
# kleine Vorstellungsrunde
#### Wie heisse ich ?
#### Habe ich Erfahrungen mit Applikationen oder Softwaretechniken ?
#### Welche Erwartungen habe ich an einen 2-stündigen Workshop mit dem Titel "Containertechnolgien" ?
#### Schon mal Begriffe wie Containertechnik oder Docker gehört ?

    
